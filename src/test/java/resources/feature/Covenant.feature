Feature: Covenant Test Set
  AS a new user
  I WANT TO login
  SO THAT I have access to the platform

  Background: Login
    Given I open "https://127.0.0.1:7443" in "Chrome" browser
    And the login page is loaded
    When I set my credentials
      | user      | password |
      | delmer176 | 123456   |
    Then I can login
    And the menu is displayed

  Scenario: Download launcher
    #Create a listener
    Given I click on Listeners menu
    And the Listeners page is displayed
    When I click on Create listener button
    And the Create Listeners page is displayed
    And I fill all the Bridge listener fields
      | name       | bindAddress | bindPort | connectPort | connectAddress | bridgeProfile    |
      | f4d6712d8d | 0.0.0.0     | 7444     | 7445        | 192.168.1.230  | TCPBridgeProfile |
    Then the Listeners page is displayed
    And I verify that the listener "f4d6712d8d" was created
    #Generate a launcher
    When I click on Launchers menu
    And the Launchers page is displayed
    When I go to generate a Binary Launcher
    And the binary launcher page is displayed
    And I generate the Binary launcher
    Then I verify that the launcher has been created
    And download the launcher file
