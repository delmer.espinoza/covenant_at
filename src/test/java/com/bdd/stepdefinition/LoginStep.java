package com.bdd.stepdefinition;

import com.bdd.page.LoginPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStep extends WebDriverManager {

    LoginPage loginPage;

    @Given("the login page is loaded")
    public void the_login_page_is_loaded() {
        loginPage = new LoginPage(driver);
    }

    @When("I set my credentials")
    public void i_set_my_user_and_password(DataTable dataTable) {
        loginPage.setUsername(dataTable);
        loginPage.setPassword(dataTable);
    }

    @Then("I can login")
    public void i_can_login() {
        loginPage.clickLogin();
    }

}
