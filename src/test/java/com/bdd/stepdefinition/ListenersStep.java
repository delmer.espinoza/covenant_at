package com.bdd.stepdefinition;

import com.bdd.page.ListenersPage;
import com.bdd.page.LoginPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class ListenersStep extends WebDriverManager {

    ListenersPage listenersPage;

    @Given("the Listeners page is displayed")
    public void listenerPageLoaded() {
        listenersPage = new ListenersPage(driver);
    }

    @When("I click on Create listener button")
    public void createListener() {
        listenersPage.clickCreate();
    }

    @Then("I verify that the listener {string} was created")
    public void verifyListenerCreated(String name){
        Assert.assertTrue("The listener has been created.", listenersPage.isListenerDisplayed(name));
    }

}
