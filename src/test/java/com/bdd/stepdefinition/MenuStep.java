package com.bdd.stepdefinition;

import com.bdd.page.MenuPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MenuStep extends WebDriverManager {

    MenuPage menuPage;

    @Then("the menu is displayed")
    public void listenerPageLoaded() {
        menuPage = new MenuPage(driver);
    }

    @Given("I click on Listeners menu")
    public void createListener() {
        menuPage.clickListenersMenu();
    }

    @When("I click on Launchers menu")
    public void i_click_on_launchers_menu() {
        menuPage.clickLaunchersMenu();
    }

}
