package com.bdd.stepdefinition;

import com.bdd.page.BinaryLauncherPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class BinaryLauncherStep extends WebDriverManager {

    BinaryLauncherPage binaryLauncherPage;

    @When("the binary launcher page is displayed")
    public void the_generate_launcher_page_is_displayed() {
        binaryLauncherPage = new BinaryLauncherPage(driver);
    }

    @When("I generate the Binary launcher")
    public void i_generate_the_binary_launcher() {
        binaryLauncherPage.setImplantTemplate();
        binaryLauncherPage.clickGenerate();
    }

    @Then("I verify that the launcher has been created")
    public void i_verify_that_the_launcher_has_been_created() {
        Assert.assertTrue("The launcher has been created!", binaryLauncherPage.launcherCreated());
    }

    @Then("download the launcher file")
    public void download_the_launcher_file() {
        binaryLauncherPage.downloadLauncher();
        Assert.assertTrue("The launcher has been created!", binaryLauncherPage.launcherDownloaded());
    }

}
