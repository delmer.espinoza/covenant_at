package com.bdd.stepdefinition;

import com.bdd.util.WebDriverManager;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class MainStep extends WebDriverManager{

    @Given("I open {string} in {string} browser")
    public void i_open_in_browser(String url, String browser) {
        openBrowser(url,browser);
    }

    @After()
    public void closeBrowser(Scenario scenario) {
        takeScreenshot(scenario);
        driver.quit();
    }

}
