package com.bdd.stepdefinition;

import com.bdd.page.LaunchersPage;
import com.bdd.page.ListenersPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class LaunchersStep extends WebDriverManager {

    LaunchersPage launchersPage;

    @When("the Launchers page is displayed")
    public void LauncherPageLoaded() {
        launchersPage = new LaunchersPage(driver);
    }

    @When("I go to generate a Binary Launcher")
    public void i_go_to_generate_a_binary_launcher() {
        launchersPage.clickBinary();
    }

}
