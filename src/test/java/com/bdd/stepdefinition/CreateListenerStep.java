package com.bdd.stepdefinition;

import com.bdd.page.CreateListenerPage;
import com.bdd.util.WebDriverManager;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.When;

public class CreateListenerStep extends WebDriverManager {

    CreateListenerPage createListenerPage;

    @When("the Create Listeners page is displayed")
    public void createListenerPageLoaded() {
        createListenerPage = new CreateListenerPage(driver);
    }

    @When("I fill all the Bridge listener fields")
    public void fillListenerData(DataTable dataTable) {
        createListenerPage.clickTabBridgeListener();
        createListenerPage.setName(dataTable);
        createListenerPage.setBindAddress(dataTable);
        createListenerPage.setBindPort(dataTable);
        createListenerPage.setConnectPort(dataTable);
        createListenerPage.setConnectAddress(dataTable);
        createListenerPage.setBridgeProfile(dataTable);
        createListenerPage.clickCreate();
    }

    public void createListener() {
        createListenerPage.clickCreate();
    }

}
