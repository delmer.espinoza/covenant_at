package com.bdd.util;

import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.Map;


public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public boolean isElementDisplayed(By by) {
        boolean isDisplayed = true;
        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(by)));
        } catch (Exception e) {
            isDisplayed = false;
        }
        return isDisplayed;
    }

    public void waitAndClick(By by) {
        wait.until(ExpectedConditions.elementToBeClickable(by));
        driver.findElement(by).click();
    }

    public String getLocatorText(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return driver.findElement(by).getText().trim();
    }

    public String getStringFromList(By by, String name) {
        String result = "";
        String elementText = "";
        List<WebElement> elementsList = driver.findElements(by);
        for (WebElement element : elementsList) {
            elementText = element.getText().trim();
            if (elementText.equals(name)) {
                result = elementText;
            }
        }
        return result;
    }

    public boolean isStringInList(By by, String name) {
        boolean result = false;
        List<WebElement> elementsList = driver.findElements(by);
        for (WebElement element : elementsList) {
            if (element.getText().trim().equals(name)) {
                result = true;
            }
        }
        return result;
    }

    public void waitClearAndSet(By by, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(value);
    }

    public void waitAndSelect(By by, String value) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        Select element = new Select(driver.findElement(by));
        element.selectByVisibleText(value);
    }

    public String getDataFromDataTable(DataTable dataTable, String header) {
        List<Map<String, String>> pairs = dataTable.asMaps(String.class, String.class);
        return pairs.get(0).get(header);
    }

    public boolean fileExists(String fileName) {
        boolean exists = false;
        File launcher;
        int cont = 0;
        while (cont<16) {
            try {
                cont++;
                Thread.sleep(1000);
                launcher = new File("src\\test\\java\\resources\\downloads", fileName + ".exe");
                exists = launcher.exists();
                if (exists) {
                    cont = 16;
                }
            } catch (Exception e) {}
        }
        return exists;
    }

}
