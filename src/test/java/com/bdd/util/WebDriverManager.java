package com.bdd.util;

import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.HashMap;
import java.util.Map;

public class WebDriverManager {

    public static WebDriver driver;

    public WebDriver setDriver(String browser) {

        switch (browser) {
            case "Firefox":
                driver = new FirefoxDriver();
                break;
            case "Chrome":
                //if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/java/resources/drivers/chromedriver.exe");
                //}else if (System.getProperty("os.name").toLowerCase().contains("linux")){
                    //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/java/resources/drivers/chromedriver");
                //}else{
                    //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/java/resources/drivers/mac/chromedriver");
                //}
                ChromeOptions options = new ChromeOptions();
                Map<String, Object> prefs = new HashMap<>();
                prefs.put("safebrowsing.enabled", "true");
                prefs.put("download.default_directory", System.getProperty("user.dir") + "\\src\\test\\java\\resources\\downloads");
                options.setExperimentalOption("prefs", prefs);
                options.addArguments("--ignore-ssl-errors=yes");
                options.addArguments("--ignore-certificate-errors");
                options.setExperimentalOption("prefs", prefs);
                driver = new ChromeDriver(options);
                break;
            default:
                driver = new InternetExplorerDriver();
                break;
        }
        return driver;
    }

    public void openBrowser(String url, String browser) {
        driver = setDriver(browser);
        driver.manage().window().maximize();
        driver.get(url);
    }

    public void takeScreenshot(Scenario scenario){
        if (scenario.isFailed()) {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", "img");
        }
    }
}
