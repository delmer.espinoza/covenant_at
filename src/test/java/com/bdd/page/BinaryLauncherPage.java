package com.bdd.page;

import com.bdd.util.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BinaryLauncherPage extends BasePage {

    private By pageheader = By.xpath("//h1[contains(.,'Binary')][contains(.,'Launcher')]");
    private By slctImplantTemplate = By.id("ImplantTemplateId");
    private By selectedOption = By.xpath("//select[@id='ImplantTemplateId']/option[@selected]");
    private By btnGenerate = By.xpath("//button[@id='generate']");
    private By txtLauncher = By.id("Launcher");
    private By btnDownload = By.id("download");

    public BinaryLauncherPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageheader));
    }

    public void setImplantTemplate(){
        if (getLocatorText(selectedOption).equals("GruntBridge")){
            waitAndSelect(slctImplantTemplate,"GruntSMB");
        }else {
            waitAndSelect(slctImplantTemplate,"GruntBridge");
        }
    }

    public void clickGenerate(){
        waitAndClick(btnGenerate);
    }

    public boolean launcherCreated()  {
        wait.until(ExpectedConditions.attributeContains(txtLauncher,"value",getLocatorText(selectedOption)));
        return driver.findElement(txtLauncher).getAttribute("value").contains(getLocatorText(selectedOption));
    }

    public void downloadLauncher(){
        waitAndClick(btnDownload);
    }

    public boolean launcherDownloaded(){
        String fileName = getLocatorText(selectedOption);
        return fileExists(fileName);
    }

}
