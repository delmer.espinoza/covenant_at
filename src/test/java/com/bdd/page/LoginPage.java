package com.bdd.page;

import com.bdd.util.BasePage;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends BasePage {

    private By txtUsername = By.id("CovenantUserRegister_UserName");
    private By txtPassword = By.id("CovenantUserRegister_Password");
    private By txtConfirmPassword = By.id("CovenantUserRegister_ConfirmPassword");
    private By btnRegister = By.xpath("//button[text()='Register']");
    private By btnLogin = By.xpath("//button[text()='Login']");

    public LoginPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(txtUsername));
    }

    public void setUsername(DataTable dataTable){
        String username = getDataFromDataTable(dataTable,"user");
        waitClearAndSet(txtUsername, username);
    }

    public void setPassword(DataTable dataTable){
        String password = getDataFromDataTable(dataTable,"password");
        waitClearAndSet(txtPassword, password);
    }

    public void setConfirmPassword(DataTable dataTable){
        String password = getDataFromDataTable(dataTable,"password");
        waitClearAndSet(txtConfirmPassword, password);
    }

    public void clickRegister(){
        waitAndClick(btnRegister);
    }

    public void clickLogin(){
        waitAndClick(btnLogin);
    }


}
