package com.bdd.page;

import com.bdd.util.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LaunchersPage extends BasePage {

    private By pageheader = By.xpath("//h1[text()='Launchers']");
    private By linkBinary = By.xpath("//a[text()='Binary']");

    public LaunchersPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageheader));
    }

    public void clickBinary(){
        waitAndClick(linkBinary);
    }

}
