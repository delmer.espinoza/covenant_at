package com.bdd.page;

import com.bdd.util.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ListenersPage extends BasePage {

    private By pageheader = By.xpath("//h1[text()='Listeners']");
    private By btnCreate = By.xpath("//div[@id='listeners']//a[contains(.,'Create')]");
    private By lsListeners = By.xpath("//td/a[contains(@href,'listener')]");

    public ListenersPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageheader));
    }

    public void clickCreate(){
        waitAndClick(btnCreate);
    }

    public boolean isListenerDisplayed(String name){
        return isStringInList(lsListeners, name);
    }

}
