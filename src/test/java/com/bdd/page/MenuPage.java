package com.bdd.page;

import com.bdd.util.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MenuPage extends BasePage {

    private By menuListeners = By.id("nav-listeners");
    private By menuLaunchers = By.id("nav-launchers");

    public MenuPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(menuListeners));
    }

    public void clickListenersMenu(){
        waitAndClick(menuListeners);
    }

    public void clickLaunchersMenu(){
        waitAndClick(menuLaunchers);
    }

}
