package com.bdd.page;

import com.bdd.util.BasePage;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CreateListenerPage extends BasePage {

    private By pageheader = By.xpath("//h1[text()='Create Listener']");
    private By tabBridge = By.id("bridge-tab");

    //Bridge tab
    private By txtName = By.xpath("//div[@id='bridge']//*[@id='Name']");
    private By txtBindAddress = By.xpath("//div[@id='bridge']//input[@id='BindAddress']");
    private By txtBindPort = By.xpath("//div[@id='bridge']//input[@id='BindPort']");
    private By txtConnectPort = By.xpath("//div[@id='bridge']//input[@id='ConnectPort']");
    private By txtConnectAddress = By.xpath("//div[@id='bridge']//input[@id='ConnectAddresses_0_']");
    private By slctBridgeProfile = By.xpath("//div[@id='bridge']//select[@id='ProfileId']");
    private By btnCreate = By.xpath("//div[@id='bridge']//button[contains(.,'Create')]");

    public CreateListenerPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageheader));
    }

    public void clickTabBridgeListener() {
        waitAndClick(tabBridge);
    }

    public void setName(DataTable dataTable) {
        String name = getDataFromDataTable(dataTable, "name");
        waitClearAndSet(txtName, name);
    }

    public void setBindAddress(DataTable dataTable) {
        String bindAddress = getDataFromDataTable(dataTable, "bindAddress");
        waitClearAndSet(txtBindAddress, bindAddress);
    }

    public void setBindPort(DataTable dataTable)
    {
        String bindPort = getDataFromDataTable(dataTable, "bindPort");
        waitClearAndSet(txtBindPort, bindPort);
    }

    public void setConnectPort(DataTable dataTable) {
        String connectPort = getDataFromDataTable(dataTable, "connectPort");
        waitClearAndSet(txtConnectPort, connectPort);
    }

    public void setConnectAddress(DataTable dataTable) {
        String connectAddress = getDataFromDataTable(dataTable, "connectAddress");
        waitClearAndSet(txtConnectAddress, connectAddress);
    }

    public void setBridgeProfile(DataTable dataTable) {
        String bridgeProfile = getDataFromDataTable(dataTable, "bridgeProfile");
        waitAndSelect(slctBridgeProfile, bridgeProfile);
    }

    public void clickCreate() {
        waitAndClick(btnCreate);
    }
}
