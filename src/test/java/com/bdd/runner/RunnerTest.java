package com.bdd.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/resources/feature"
        ,glue = "com.bdd.stepdefinition"
        //,tags = "@case02"
        ,plugin = {"pretty", "html:target/cucumber-html-report.html", "json:target/cucumber-json-report.json" }
)
public class RunnerTest {
}
